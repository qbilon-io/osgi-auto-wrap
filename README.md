# OSGi Auto Wrap
This repository contains some code snippets that can help you to automate tedious steps when wrapping Java libraries that do not have a proper OSGi manifest.

## What Problem does this code solve?
When you are new to OSGi one of the problems you will often encounter is that one or more libraries you would like to use do not have a proper OSGi manifest and therefore could not be used properly in an OSGi runtime.
Usually when you encounter this problem, the suggested solution you find online, tells you to rebundle/rewrap the respective library, this usually means that
1) You unpack the jar you want to wrap and then put its contents into a new jar where you generate a proper MANIFEST.MF file for, or
2) You put the library jar into another jar (like a shell) and generate a proper MANIFEST.MF file for that outer jar.

However there are numerous problems that you can encounter when wrapping non-OSGi libraries.
1) Often the library does not consist of only one jar and you end up wrapping also dependencies of that library, because chances are high those do not have a proper MANIFEST.MF file either
2) The Library you wrap uses dynamic Class-Loading, e.g., calls `Class.forName("SomeName)` somewhere in its depths and the successfully wrapped library crashes at runtime, because the newly created bundle (and its classloader) does not know about that dynamically loaded class.
3) The library uses Java's `ServiceLoader` mechanism to plug in additional code and your newly wrapped library breaks at runtime, because the classloader is not able to find the services definitions that would be found on a flat classpath


I myself encountered those problems several times and I always struggled to find a good, reproducible way to get those libraries to work in my environment.

So I finally came up with a process that works well for me and I tried to automate major parts of it, so that I can try different rewrappings fast.

## How to use it
This code can be executed as a JBang command (`jbang wrap@qbilon-io`). For more information on jbang see https://www.jbang.dev/.

In the following I will show you the steps you need to do and where the code helps you:

1) Copy all maven artifacts you want to wrap into the `dependencies` section of the pom.xml in the `src/main/resources` folder
2) Run the main method of `Main.java` and just hit enter when asked for the path to the pom file you would like to use (default is that pom file in the resources folder)

What the code now does is to use `mvn dependency:tree` to get all transitive dependencies of the root artifacts you provided.
It then uses `mvn dependency:copy-dependencies` to copy all those artifacts (jars) into the `src/main/resources/target/dependency` folder.
After that it inspects each artifact in that folder and creates several things:
* a set of all packages within that artifact
* a flag indicating if this artifact has a MANIFEST.MF file AND if this MANIFEST.MF file has a `Bundle-ManifestVersion` header present (this usually means that this jar is already a bundle that can be used in a OSGi runtime)
* a set of services this artifact provides via its `META-INF/services` folder (those are services that could be loaded via `ServiceLoader.load(SomeClass.class)`)
Finally the code prints the augmented dependency tree with all transitive dependencies and the information if it is already a bundle and if it does provide services (and which).

Now it prompts you to provide it with a file that will contain all the dependencies you want to wrap (e.g. you only want to wrap a subpart of its transitive dependencies and provide the reat by other means or rebundle them seperately)
For this:

3) Copy the printed Augmented Dependency Tree into `src/main/resources/wrapdeps.txt` file and delete the lines with dependencies you don't want to include in this wrapped bundle
4) After that the code asks you which packages you want to initially export. Usually you package a library which has the package name x.abx.d (e.g. com.azure.core...) and want to export all packages that start with this name. Thus you can type `com.azure.core*` with a wildcard at its end. This "prefix" is taken by the code and turned into all possible packages (excluding those that have name segments contained in those provided by step 5) that are contained within the provided artifacts.
5) Finally you are asked which package segment names you don't allow in your initial export (default is: impl, implementation,internal) because those are expected to not be part of the api of a jar.

What the code now does is run `mvn package`. This triggers the execution of the Maven Bundle Plugin that is configured in the pom.xml. This plugin now tries to create a wrapped version of all the transitive libraries you provided in step 3 with the exports you provided in step 4 minus exports that contain a package segment provided in step 5.
Sometimes this plugin encounters the case that one of those exported packages has a dependency to another package provided by one of the other provided artifacts. In that case we have to add this package to our exports (now there can also be packages with segments that were initially forbidden, because not every library auther works perfectly clean) and again run `mvn package` which again triggers the Maven Bundle Plugin but now with more exports. This process is repeated until ther is no more package found that has to be added to the exports.

6) The code has now created an extesive list of imports/exports and included artifacts in the pom.xml file. You only need to run `mvn clean install` in the resources folder to generate the latest version of the MANIFEST.MF file for your newly created bundle.

Optional but sometimes needed:

7) Finetune Manifest generation
    * inspect manifest imports for packages that might come from excluded artifacts (step 4) and make their imports non-optional
    * inspect manifest for additional imports that should be excluded, e.g., dalvik*, android*, etc.
8) adapt pom according to step 7
9) Run the wrapped library in OSGi and see if there are some errors at runtime. Sometime you will encounter a "ClassDefNotFoundException". In that case, take the package name of that class and put it into the import section of the Maven Bundle Plugin.

## Example: Making Azure-Core Library OSGi ready
As an example we will wrap the azure-core library together with its default http implementation which uses netty.

### Step 1
We add the needed dependencies to the pom.xml file:
```xml
<dependency>
    <groupId>com.azure</groupId>
    <artifactId>azure-core</artifactId>
    <version>1.35.0</version>
</dependency>
<dependency>
    <groupId>com.azure</groupId>
    <artifactId>azure-core-http-netty</artifactId>
    <version>1.12.0</version>
</dependency>
```
### Step 2 
Run the main method
### Step 3 
We copied the augmented dependency tree into wrapdeps.txt:
```
+- com.azure:azure-core:jar:1.35.0:compile                                                         -         
|  +- com.fasterxml.jackson.core:jackson-annotations:jar:2.13.4:compile                            B         
|  +- com.fasterxml.jackson.core:jackson-core:jar:2.13.4:compile                                   B         com.fasterxml.jackson.core.JsonFactory
|  +- com.fasterxml.jackson.core:jackson-databind:jar:2.13.4.2:compile                             B         com.fasterxml.jackson.core.ObjectCodec
|  +- com.fasterxml.jackson.datatype:jackson-datatype-jsr310:jar:2.13.4:compile                    B         com.fasterxml.jackson.databind.Module
|  +- com.fasterxml.jackson.dataformat:jackson-dataformat-xml:jar:2.13.4:compile                   B         com.fasterxml.jackson.core.JsonFactory
                                                                                                             com.fasterxml.jackson.core.ObjectCodec
|  |  \- org.codehaus.woodstox:stax2-api:jar:4.2.1:compile                                         B         
|  +- com.fasterxml.woodstox:woodstox-core:jar:6.4.0:compile                                       B         com.ctc.wstx.shaded.msv.relaxng_datatype.DatatypeLibraryFactory
                                                                                                             javax.xml.stream.XMLEventFactory
                                                                                                             javax.xml.stream.XMLInputFactory
                                                                                                             javax.xml.stream.XMLOutputFactory
                                                                                                             org.codehaus.stax2.validation.XMLValidationSchemaFactory
                                                                                                             org.codehaus.stax2.validation.XMLValidationSchemaFactory.dtd
                                                                                                             org.codehaus.stax2.validation.XMLValidationSchemaFactory.relaxng
                                                                                                             org.codehaus.stax2.validation.XMLValidationSchemaFactory.w3c
|  +- org.slf4j:slf4j-api:jar:1.7.36:compile                                                       B         
|  \- io.projectreactor:reactor-core:jar:3.4.26:compile                                            B         reactor.blockhound.integration.BlockHoundIntegration
|     \- org.reactivestreams:reactive-streams:jar:1.0.4:compile                                    B         
\- com.azure:azure-core-http-netty:jar:1.12.0:compile                                              -         com.azure.core.http.HttpClientProvider
   +- io.netty:netty-handler:jar:4.1.76.Final:compile                                              B         
   |  +- io.netty:netty-common:jar:4.1.76.Final:compile                                            B         reactor.blockhound.integration.BlockHoundIntegration
   |  +- io.netty:netty-resolver:jar:4.1.76.Final:compile                                          B         
   |  +- io.netty:netty-transport:jar:4.1.76.Final:compile                                         B         
   |  \- io.netty:netty-codec:jar:4.1.76.Final:compile                                             B         
   +- io.netty:netty-handler-proxy:jar:4.1.76.Final:compile                                        B         
   |  \- io.netty:netty-codec-socks:jar:4.1.76.Final:compile                                       B         
   +- io.netty:netty-buffer:jar:4.1.76.Final:compile                                               B         
   +- io.netty:netty-codec-http:jar:4.1.76.Final:compile                                           B         
   +- io.netty:netty-codec-http2:jar:4.1.76.Final:compile                                          B         
   +- io.netty:netty-transport-native-unix-common:jar:4.1.76.Final:compile                         B         
   +- io.netty:netty-transport-native-epoll:jar:linux-x86_64:4.1.76.Final:compile                  B         
   |  \- io.netty:netty-transport-classes-epoll:jar:4.1.76.Final:compile                           B         
   +- io.netty:netty-transport-native-kqueue:jar:osx-x86_64:4.1.76.Final:compile                   B         
   |  \- io.netty:netty-transport-classes-kqueue:jar:4.1.76.Final:compile                          B         
   +- io.netty:netty-tcnative-boringssl-static:jar:2.0.51.Final:compile                            B         
   |  +- io.netty:netty-tcnative-classes:jar:2.0.51.Final:compile                                  B         
   |  +- io.netty:netty-tcnative-boringssl-static:jar:linux-x86_64:2.0.51.Final:compile            B         
   |  +- io.netty:netty-tcnative-boringssl-static:jar:linux-aarch_64:2.0.51.Final:compile          B         
   |  +- io.netty:netty-tcnative-boringssl-static:jar:osx-x86_64:2.0.51.Final:compile              B         
   |  +- io.netty:netty-tcnative-boringssl-static:jar:osx-aarch_64:2.0.51.Final:compile            B         
   |  \- io.netty:netty-tcnative-boringssl-static:jar:windows-x86_64:2.0.51.Final:compile          B         
   \- io.projectreactor.netty:reactor-netty-http:jar:1.0.18:compile                                B         
      +- io.netty:netty-resolver-dns:jar:4.1.75.Final:compile                                      B         
      |  \- io.netty:netty-codec-dns:jar:4.1.75.Final:compile                                      B         
      +- io.netty:netty-resolver-dns-native-macos:jar:osx-x86_64:4.1.75.Final:compile              B         
      |  \- io.netty:netty-resolver-dns-classes-macos:jar:4.1.75.Final:compile                     B         
      \- io.projectreactor.netty:reactor-netty-core:jar:1.0.18:compile                             B 
```

Now we trim down the dependency tree to those dependecies we really need.
1) all the fasterxml stuff is already an OSGi bundle (indicated by the `B`). Therefore we exclude it and can install those bundles seperately in the runtime.
2) the same goes for the whole netty stuff, reactor, reactivestreams and slf4j

This leaves us with actually two dependencies:
```
+- com.azure:azure-core:jar:1.35.0:compile
\- com.azure:azure-core-http-netty:jar:1.12.0:compile                                              -         com.azure.core.http.HttpClientProvider
```
We could bundle the seperately, as both are no actual bundles but additionally azure-core-http-netty provides a service via serviceloader that seems to be a service that is picked up by the azure-core bundle (judging by the name).
In order to make sure that the serviceloader works, we bundle those two together. This way the classloader for the azure-core jar will also see the azure-core-http-netty jar and find the service file in META-INF/services.

Thus we only copy the above two lines into the wrapdpes.txt.

### Step 4
We only want to use the azure-core classes, therefore we provide `com.azure.core*`

### Step 5
impl/internal/implementation seem good enough to us. so we just hit enter, as those are the defaults to be excluded

This provides us with a pom.xml like this:
```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd" xmlns="http://maven.apache.org/POM/4.0.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <modelVersion>4.0.0</modelVersion>
  <groupId>io.qbilon</groupId>
  <artifactId>osgi-wrap</artifactId>
  <version>1.0.0-SNAPSHOT</version>
  <packaging>bundle</packaging>
  <name>QBILON :: OSGi :: WRAP</name>
  <description>This OSGi bundle simply wraps the given dependencies</description>
  <dependencies>
    <dependency>
      <groupId>com.azure</groupId>
      <artifactId>azure-core</artifactId>
      <version>1.35.0</version>
    </dependency>
    <dependency>
      <groupId>com.azure</groupId>
      <artifactId>azure-core-http-netty</artifactId>
      <version>1.12.0</version>
    </dependency>
  </dependencies>
  <build>
    <plugins>
      <plugin>
        <groupId>org.apache.felix</groupId>
        <artifactId>maven-bundle-plugin</artifactId>
        <version>4.2.1</version>
        <extensions>true</extensions>
        <dependencies>
          <dependency>
            <groupId>biz.aQute.bnd</groupId>
            <artifactId>biz.aQute.bndlib</artifactId>
            <version>6.1.0</version>
          </dependency>
        </dependencies>
        <configuration>
          <instructions>
            <Bundle-SymbolicName>${project.artifactId}</Bundle-SymbolicName>
            <Bundle-Version>${project.version}</Bundle-Version>
            <_dsannotations>*</_dsannotations>
            <Import-Package>
                !com.azure.core.annotation,
                !com.azure.core.client.traits,
                !com.azure.core.credential,
                !com.azure.core.cryptography,
                !com.azure.core.exception,
                !com.azure.core.http,
                !com.azure.core.http.netty,
                !com.azure.core.http.policy,
                !com.azure.core.http.rest,
                !com.azure.core.models,
                !com.azure.core.util,
                !com.azure.core.util.builder,
                !com.azure.core.util.io,
                !com.azure.core.util.logging,
                !com.azure.core.util.metrics,
                !com.azure.core.util.paging,
                !com.azure.core.util.polling,
                !com.azure.core.util.serializer,
                !com.azure.core.util.tracing,
                *;resolution:=optional
            </Import-Package>
            <Private-Package></Private-Package>
            <Embed-Transitive>true</Embed-Transitive>
            <Embed-Dependency>*;scope=compile|runtime|provided;inline=false;artifactId=
                azure-core|
                azure-core-http-netty
            </Embed-Dependency>
            <_exportcontents>
                com.azure.core.annotation,
                com.azure.core.client.traits,
                com.azure.core.credential,
                com.azure.core.cryptography,
                com.azure.core.exception,
                com.azure.core.http,
                com.azure.core.http.netty,
                com.azure.core.http.policy,
                com.azure.core.http.rest,
                com.azure.core.models,
                com.azure.core.util,
                com.azure.core.util.builder,
                com.azure.core.util.io,
                com.azure.core.util.logging,
                com.azure.core.util.metrics,
                com.azure.core.util.paging,
                com.azure.core.util.polling,
                com.azure.core.util.serializer,
                com.azure.core.util.tracing
            </_exportcontents>
          </instructions>
        </configuration>
      </plugin>
    </plugins>
  </build>
</project>
```

This pom.xml can now be adapted to your needs and the resulting bundle can be installed in an OSGi runtime :)


## Ideas For Possible Future Enheancements
* when creating the imports we could scan the resulting import statements and those that are optional and within  artifacts that have not been included in the wrap could be made non-optional automatically
* when a package is exported via transitive export and it comes from a jar that is not a bundle, then we could automatically provide it with a ;version=x.y.z statement where the version comes from the artifact -> version range?
* We could scan the sourcecode of all transitive depdencies for usages of ServiceLoader.load()/Class.ForName()/and similar stuff and augment the dpendency tree with this information
    * This might lead to automatical SPI-Provider/Consumer header information
* We could scan the resulting import statements for common JRE dependencies and make them non-optional
    * e.g., something like javax... or sun.misc stuff