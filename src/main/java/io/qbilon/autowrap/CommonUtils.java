package io.qbilon.autowrap;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class CommonUtils {
    public static List<AugmentedDependency> dependencies(InputStream is) {
        List<AugmentedDependency> deps = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(is))) {
            String line = null;
            while ((line = reader.readLine()) != null) {
                if (line.contains("+- ") || line.contains("\\- ")) {
                    deps.add(new AugmentedDependency(line));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return deps;
    }
}
