package io.qbilon.autowrap;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * This step provides a augmented dependency tree for a pom.xml that lies within src/main/resources
 * For this this step 
 * - first retrieves all transitive dependencies in all scopes
 * - downloads all the dependencies
 * - scans the downloaded dependencies for provided services (META-INF/services folder is present)
 * - scans the downloaded dependencies for an OSGi manifest (META-INF/MANIFEST.MF contains a Bundle-ManifestVersion)
 * - prints an augmented version of the original dependency tree with information if the dependency is a bundle and which services it provides
 */
public class Step1_AugmentDependencies {

    public List<AugmentedDependency> augmentDependencyTree(Path pathToPom) {
        System.out.println("Creating augmented dependencies for pom.xml in " + pathToPom.toAbsolutePath() + ". This might take a few seconds...");
        // first determine the dependencies
        List<AugmentedDependency> dependencies = dependencies(pathToPom);
        // then augment them
        augmentDepencies(dependencies, pathToPom);
        return dependencies;
    }

    private List<AugmentedDependency> dependencies(Path pathToPom) {
        try {
            System.out.println("Calculating transitive depdendencies.");
            Process depTreeProcess = Runtime.getRuntime().exec("mvn clean dependency:tree", null, pathToPom.toFile());
            try (InputStream is = depTreeProcess.getInputStream()) {
                return CommonUtils.dependencies(is);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    private void printOutput(Process p) {
        try (InputStream is = p.getInputStream()) {
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(is))) {
                String line = null;
                while ((line = reader.readLine()) != null) {
                    System.out.println(line);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void augmentDepencies(List<AugmentedDependency> dependencies, Path pathToPom) {
        try {
            System.out.println("Copying transitive dependencies into target/dependency/");
            // fetch all dependencies and wait for process to finish
            Process p = Runtime.getRuntime().exec("mvn clean dependency:copy-dependencies", null, pathToPom.toFile()).onExit().get();
            if(p.exitValue() != 0) {
                printOutput(p);
                System.exit(0);
            }

            System.out.println("Creating augmented dependency tree...");
            int maxLineLength = 0;
            // those dependencies are now under pathToPom/target/dependency
            // now we have to read each one and augment it with information regardin bundle
            // and services
            for (AugmentedDependency dependency : dependencies) {
                // filenames are in the form <artifact-id>-<version>-<OPTIONAL:platform>.jar
                String pathToJar = pathToPom.toAbsolutePath() + "/target/dependency/" + dependency.getJarName();
                JarFile jarFile = new JarFile(pathToJar);

                Enumeration<JarEntry> entries = jarFile.entries();
                while (entries.hasMoreElements()) {
                    JarEntry entry = entries.nextElement();
                    String name = entry.getName();

                    augmentServiceIfPresent(dependency, name);
                    augmentBundleHeaderIfPresent(dependency, jarFile, entry, name);
                    augmentPackages(dependency, name);
                }

                if (dependency.rawLine.length() > maxLineLength) {
                    maxLineLength = dependency.rawLine.length();
                }
            }

            System.out.println();
            System.out.println("Augmented Dependency Tree:");

            for (AugmentedDependency dependency : dependencies) {
                String bundle = dependency.hasBundleHeader ? "B" : "-";
                prettyPrint(dependency.rawLine, maxLineLength, bundle, dependency.services);
            }

        } catch (IOException | InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    private void augmentPackages(AugmentedDependency dependency, String name) {
        if(name.endsWith(".class")) {
            // It's a class file -> its folder is a package
            String pkg = null;
            if(name.lastIndexOf("/") > -1) {
                pkg = name.substring(0, name.lastIndexOf("/")).replace("/", ".");
            } else {
                // there is no hierarchy
                pkg = name;
            }
            dependency.addPackage(pkg);
        }
    }

    public void prettyPrint(String base, int maxLength, String bundle, Set<String> services) {
        int len1 = maxLength + 10;
        int len2 = maxLength + 20;
        String pretty = null;
        if (services.size() <= 1) {
            pretty = padRightSpaces(padRightSpaces(base, len1) + bundle, len2) + String.join(",", services);
        } else {
            int i = 0;
            for (String service : services) {
                if (i == 0) {
                    pretty = padRightSpaces(padRightSpaces(base, len1) + bundle, len2) + service;
                } else {
                    pretty = pretty + "\n" + padRightSpaces("", len2) + service;
                }
                i++;
            }
        }
        System.out.println(pretty);
    }

    public String padRightSpaces(String inputString, int length) {
        if (inputString.length() == length) {
            return inputString;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(inputString);
        while (sb.length() < length) {
            sb.append(' ');
        }

        return sb.toString();
    }

    public void augmentServiceIfPresent(AugmentedDependency dependency, String name) {
        String servicePath = "META-INF/services/";
        // check for services
        if (name.startsWith(servicePath)) {
            if (name.length() > servicePath.length()) {
                // there is a service file -> extract its name
                dependency.addService(name.substring(servicePath.length()));
            }
        }
    }

    public void augmentBundleHeaderIfPresent(AugmentedDependency dep, JarFile file, JarEntry entry, String name) {
        String manifestPath = "META-INF/MANIFEST.MF";
        if (name.equals(manifestPath)) {
            try (InputStream is = file.getInputStream(entry);
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is))) {
                String line = null;
                while ((line = reader.readLine()) != null) {
                    if (line.startsWith("Bundle-ManifestVersion")) {
                        dep.hasBundleHeader = true;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
