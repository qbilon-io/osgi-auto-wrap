//SOURCES Step1_AugmentDependencies.java
//SOURCES CommonUtils.java
//SOURCES AugmentedDependency.java
//DEPS org.apache.maven:maven-model-builder:3.8.7

package io.qbilon.autowrap;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class Tree {
    
    public static void main(String[] args) {
        Path currentDirectory = Paths.get("");
        Scanner scn = new Scanner(System.in);
        System.out.println("Current execution directoy: " + currentDirectory.toAbsolutePath().toString());
        
        String pathToPomFolder = "pom.xml";
        Path pomPath = getPathInput(scn, "Provide relative path to wrap pom.xml that contains the dependencies to be wrapped. Default: " + pathToPomFolder, pathToPomFolder);
        
        Step1_AugmentDependencies tree = new Step1_AugmentDependencies();
        tree.augmentDependencyTree(pomPath.getParent());

        scn.close();
    }

    private static Path getPathInput(Scanner scn, String print, String defaultValue) {
        String result = getStringInput(scn, print, defaultValue);
        return Paths.get(result).toAbsolutePath();
    }

    private static String getStringInput(Scanner scn, String print, String defaultValue) {
        String result = defaultValue;
        System.out.println(print);
        String input = scn.nextLine();
        if(input != null && !input.isBlank()) {
            result = input;
        }
        System.out.println();
        return result;
    }
}
