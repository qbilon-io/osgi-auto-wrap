package io.qbilon.autowrap;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.maven.model.Model;
import org.apache.maven.model.Plugin;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.apache.maven.model.io.xpp3.MavenXpp3Writer;
import org.codehaus.plexus.util.xml.Xpp3Dom;

public class Step2_GenerateWrapperPom {
    
    public void generateWrapperPom(List<AugmentedDependency> allDependencies, Path wrapperPomPath, Path wrapDepsPath, List<String> rootExports, List<String> excludedPkgSegmments) {
        List<AugmentedDependency> depsToWrap = null;
        try (InputStream is = new FileInputStream(wrapDepsPath.toFile())) {
            depsToWrap = dependenciesToWrap(allDependencies, is);

            Model model = readPom(wrapperPomPath);
            
            Plugin mbPlugin = model.getBuild().getPlugins().stream().filter(plugin -> plugin.getArtifactId().equals("maven-bundle-plugin")).findFirst().get();
            
            Xpp3Dom config = (Xpp3Dom) mbPlugin.getConfiguration();
            Xpp3Dom instructions = config.getChild("instructions");

            System.out.println();
            System.out.println("Initializing pom.xml for wrapping");
            initInstructions(instructions, depsToWrap, rootExports, excludedPkgSegmments);
            System.out.println("Writing initialized pom to file " + wrapperPomPath.toAbsolutePath().toString());
            writePom(wrapperPomPath, model);
            System.out.println("Starting iterative addition of transitive exports due to private packages");
            Set<String> exports = iterateTransitiveExports(wrapperPomPath, model, instructions, rootExports);
            System.out.println("Creating import section");
            createImportSection(wrapperPomPath, model, instructions, exports);
            System.out.println("FINISHED WRAPPING!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private List<AugmentedDependency> dependenciesToWrap(List<AugmentedDependency> allDependencies, InputStream is) {
        // first parse the given dependencies and then take their augemnted counterpart from allDependencies
        List<AugmentedDependency> parsedDependencies = CommonUtils.dependencies(is);
        Map<String, AugmentedDependency> allDependenciesByName = allDependencies.stream().collect(Collectors.toMap(dep -> dep.toString(), dep -> dep));
        List<AugmentedDependency> result = new ArrayList<>();
        for (AugmentedDependency dep : parsedDependencies) {
            result.add(allDependenciesByName.get(dep.toString()));
        }
        return result;
    }

    private void createImportSection(Path wrapperPomFile, Model model, Xpp3Dom instructions, Set<String> exports) {
        // exclude all exports from the imports
        // add the rest as resolution:=optional
        overWriteValue(getOrCreateChild(instructions, "Import-Package"), importString(exports));
        writePom(wrapperPomFile, model);
    }

    private String importString(Set<String> exports) {
        StringBuilder sb = new StringBuilder();
        for (String export : exports) {
            sb.append("\n" + tab(4) + "!" + export + ",");
        }
        sb.append("\n" + tab(4) + "*;resolution:=optional");
        sb.append("\n" + tab(3));
        return sb.toString();
    }

    private Model readPom(Path wrapperPomFile) {
        Model model = null;
        MavenXpp3Reader reader = new MavenXpp3Reader();
        try (FileInputStream fis =new FileInputStream(wrapperPomFile.toFile())) {
            model = reader.read(fis);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return model;
    }

    private void writePom(Path wrapperPomFile, Model model) {
        MavenXpp3Writer writer = new MavenXpp3Writer();
        try (OutputStream os = new FileOutputStream(wrapperPomFile.toFile())) {
            writer.write(os, model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Set<String> iterateTransitiveExports(Path wrapperPomFile, Model model, Xpp3Dom instructions, List<String> rootExports) {
        // we need to create the minimal subset of exports for the given rootExports that does not create a warning for private references anymore when built
        boolean needsMoreExports = true;
        SortedSet<String> transitiveExports = new TreeSet<>();
        
        while(needsMoreExports) {
            Set<String> missingExports = findMissingExports(wrapperPomFile);
            if(missingExports.size() == 0) {
                System.out.println("Found no more missing exports -> Finish!");
                needsMoreExports = false;
            } else {
                System.out.println("Found more needed transitive exports:\n\t" + String.join(",\n\t", missingExports));
                System.out.println("Adding missing exports to _exportcontents section");
                transitiveExports.addAll(missingExports);
                // rewrite export section
                overWriteValue(getOrCreateChild(instructions, "_exportcontents"), exportString(rootExports, transitiveExports));
                // write pom to file
                writePom(wrapperPomFile, model);
                System.out.println("Recreated pom.xml. Starting new iteration...");
                System.out.println();
            }
        }

        // now finally add the rootExports
        transitiveExports.addAll(rootExports);
        return transitiveExports;
    }
    
    private String exportString(List<String> rootExports, SortedSet<String> transitiveExports) {
        StringBuilder sb = new StringBuilder();
        for (String export : rootExports) {
            sb.append("\n" + tab(4) + export + ",");
        }
        sb.append("\n");
        for (String export : transitiveExports) {
            sb.append("\n" + tab(4) + export + ",");
        }
        sb.append("\n" + tab(3));
        return sb.toString();
    }

    private String tab(int count) {
        int spaceCount = count*4;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < spaceCount; i++) {
            sb.append(" ");
        }
        return sb.toString();
    }

    private Pattern missingExportsPattern = Pattern.compile("\\[(.*?)\\]");
    private Set<String> findMissingExports(Path wrapperPomFile) {
        // [WARNING] Bundle xx:yy:bundle:1.0.0-SNAPSHOT : Export a.b.c,  has 2,  private references [reactor.core, reactor.core.publisher]
        Set<String> missingExports = new HashSet<>();
        try {
            Process depTreeProcess = Runtime.getRuntime().exec("mvn package", null, wrapperPomFile.getParent().toFile());
            try (InputStream is = depTreeProcess.getInputStream()) {
                try (BufferedReader reader = new BufferedReader(new InputStreamReader(is))) {
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        if (line.contains("[WARNING] Bundle ") && line.contains(" : Export ") && line.contains("private references [") ) {
                            Matcher m = missingExportsPattern.matcher(line.replace("[WARNING] Bundle ", ""));
                            while (m.find()) {
                                String missingExportsString = m.group(1);
                                missingExports.addAll(Stream.of(missingExportsString.split(",")).map(export -> export.trim()).collect(Collectors.toSet()));
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return missingExports;
    }

    public void initInstructions(Xpp3Dom instructions, List<AugmentedDependency> depsToWrap, List<String> rootExports, List<String> excludedPkgSegments) {
        // make sure we have a minimal set of instructions available
        // <Bundle-SymbolicName>${project.artifactId}</Bundle-SymbolicName>
        // <Bundle-Version>${project.version}</Bundle-Version>
        // <_dsannotations>*</_dsannotations>
        // <Import-Package/>
        // <Private-Package/>
        // <Embed-Transitive>true</Embed-Transitive>

        setValueIfNotPresent(getOrCreateChild(instructions, "Bundle-SymbolicName"), "${project.artifactId}");
        setValueIfNotPresent(getOrCreateChild(instructions, "Bundle-Version"), "${project.version}");
        setValueIfNotPresent(getOrCreateChild(instructions, "_dsannotations"), "*");
        setValueIfNotPresent(getOrCreateChild(instructions, "Import-Package"), "");
        setValueIfNotPresent(getOrCreateChild(instructions, "Private-Package"), "");
        setValueIfNotPresent(getOrCreateChild(instructions, "Embed-Transitive"), "true");

         // we have to initially set the artifactIds to be included in the wrap 
         String embeddableArtifactIds = "\n" + tab(4) + String.join("|\n" + tab(4), depsToWrap.stream().map(dep -> dep.artifactId).distinct().sorted().collect(Collectors.toList())) + "\n" + tab(3);
         overWriteValue(getOrCreateChild(instructions, "Embed-Dependency"), "*;scope=compile|runtime|provided;inline=false;artifactId=" + embeddableArtifactIds);

         // we have to initially set the root exports for this wrap
         expandRootExports(depsToWrap, rootExports, excludedPkgSegments);
         overWriteValue(getOrCreateChild(instructions, "_exportcontents"), String.join(",", rootExports));
    }

    private void expandRootExports(List<AugmentedDependency> depsToWrap, List<String> rootExports,
            List<String> excludedPkgSegments) {
        // turn each rootExport that ends with '*' into all its possible expanded forms
        // exclude all packages that contain one of the excludedpkgsegments
        Set<String> allExpandedRootExports = new TreeSet<>();
        for (String export : rootExports) {
            if(export.endsWith("*")) {
                // expand it 
                String baseExport = export.replace("*", "");
                // search in all dependencies for fitting packages
                for (AugmentedDependency dep : depsToWrap) {
                    for (String pkg : dep.packages) {
                        if(pkg.startsWith(baseExport)) {
                            // make sure it is not a frobidden package
                            String[] segments = pkg.split("\\.");
                            boolean forbidden = false;
                            for (String forbiddenPkg : excludedPkgSegments) {
                                for(String segment : segments) {
                                    if(segment.equals(forbiddenPkg)) {
                                        forbidden = true;
                                        break;
                                    }
                                }
                                if(forbidden) {
                                    break;
                                }
                            }
                            if(!forbidden) {
                                allExpandedRootExports.add(pkg);
                            }
                        }
                    }
                }
            }
        }
        rootExports.clear();
        // replace rootExports with expanded forms
        rootExports.addAll(allExpandedRootExports);

        System.out.println("All expanded Root Exports:");
        for (String string : allExpandedRootExports) {
            System.out.println(tab(1) + string);
        }
    }

    public void setValueIfNotPresent(Xpp3Dom node, String value) {
        if(node.getValue() == null || node.getValue().isBlank()) {
            node.setValue(value);
        }
    }

    public void overWriteValue(Xpp3Dom node, String value) {
        node.setValue(value);
    }

    public Xpp3Dom getOrCreateChild(Xpp3Dom parent, String childName) {
        Xpp3Dom child = parent.getChild(childName);
        if(child == null) {
            // create child
            child = new Xpp3Dom(childName);
            parent.addChild(child);
        }
        return child;
    }
}
