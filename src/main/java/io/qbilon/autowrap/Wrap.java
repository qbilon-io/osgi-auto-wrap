//SOURCES Step1_AugmentDependencies.java
//SOURCES Step2_GenerateWrapperPom.java
//SOURCES CommonUtils.java
//SOURCES AugmentedDependency.java
//DEPS org.apache.maven:maven-model-builder:3.8.7

package io.qbilon.autowrap;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Wrap {

    public static void main(String[] args) {
        Path currentDirectory = Paths.get("");
        Scanner scn = new Scanner(System.in);

        System.out.println();
        System.out.println("####################");
        System.out.println("#      STEP 1      #");
        System.out.println("####################");
        System.out.println();
        System.out.println("Current execution directoy: " + currentDirectory.toAbsolutePath().toString());
        
        String pathToPomFolder = "pom.xml";
        Path pomPath = getPathInput(scn, "Provide relative path to wrap pom.xml that contains the dependencies to be wrapped. Default: " + pathToPomFolder, pathToPomFolder);
        
        Step1_AugmentDependencies tree = new Step1_AugmentDependencies();
        List<AugmentedDependency> dependencies = tree.augmentDependencyTree(pomPath.getParent());

        System.out.println();
        System.out.println("####################");
        System.out.println("#      STEP 2      #");
        System.out.println("####################");
        System.out.println();

        String pathToWrapDeps = "wrapdeps.txt";
        Path wrapDepsPath = getPathInput(scn,"Provide relative path to wrapdeps.txt. Default: " + pathToWrapDeps, pathToWrapDeps);

        String rootImportString = getStringInput(scn,"Provide all root imports separated by a comma, e.g., 'io.qbilon*, com.google.code.gson, etc'. Default: *", "*");
        List<String> rootImports = Stream.of(rootImportString.split(",")).map(rootImport -> rootImport.trim()).distinct().sorted().collect(Collectors.toList());

        String excludedPackageSegmentsString = getStringInput(scn,"Provide all package segments that should be excluded separated by a comma, e.g., 'internal, impl, etc'. Default: internal, impl, implementation", "internal, impl, implementation");
        List<String> excludedPackageSegments = Stream.of(excludedPackageSegmentsString.split(",")).map(excludedPkgSegment -> excludedPkgSegment.trim()).distinct().sorted().collect(Collectors.toList());
        
        Step2_GenerateWrapperPom step2 = new Step2_GenerateWrapperPom();
        step2.generateWrapperPom(dependencies, pomPath, wrapDepsPath, rootImports, excludedPackageSegments);

        scn.close();
    }

    private static Path getPathInput(Scanner scn, String print, String defaultValue) {
        String result = getStringInput(scn, print, defaultValue);
        return Paths.get(result).toAbsolutePath();
    }

    private static String getStringInput(Scanner scn, String print, String defaultValue) {
        String result = defaultValue;
        System.out.println(print);
        String input = scn.nextLine();
        if(input != null && !input.isBlank()) {
            result = input;
        }
        System.out.println();
        return result;
    }
    
}
