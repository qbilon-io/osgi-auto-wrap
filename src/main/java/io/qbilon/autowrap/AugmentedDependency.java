package io.qbilon.autowrap;

import java.util.Set;
import java.util.TreeSet;

public class AugmentedDependency {
    public String rawLine;
    public String groupId;
    public String artifactId;
    public String version;
    public String type;
    public String scope;
    public String platform;
    public Set<String> services = new TreeSet<>();
    public Set<String> packages = new TreeSet<>();
    public boolean hasBundleHeader;

    public AugmentedDependency(String line) {
        this.rawLine = line.replace("[INFO] ", "");
        parseDependency(line);
    }

    public void addService(String service) {
        services.add(service);
    }

    public void addPackage(String pkg) {
        packages.add(pkg);
    }
    private void parseDependency(String line) {
        // we assume this line starts with ' +- ' or ' \- '
        int index = line.indexOf(" +- ");
        if (index == -1) {
            index = line.indexOf(" \\- ");
        }
        // String might contain more than the dependency, e.g., B/- or services
        String possibleAugmentedDependency = line.substring(index + 4);
        String dependency = possibleAugmentedDependency.split(" ")[0];
        String[] segments = dependency.split(":");

        groupId = segments[0];
        artifactId = segments[1];
        type = segments[2];
        if (segments.length == 5) {
            // normal lib
            platform = null;
            version = segments[3];
            scope = segments[4];
        } else {
            // platform native lib
            platform = segments[3];
            version = segments[4];
            scope = segments[5];
        }
    }

    public String getJarName() {
        String platform = (this.platform != null) ? "-" + this.platform : "";
        return artifactId + "-" + version + platform + ".jar";
    }

    public String toString() {
        String platform = (this.platform != null) ? "-" + this.platform : "";
        return groupId + ":" + artifactId + ":" + version + ":" + type + ":" + scope + platform;
    }
}
